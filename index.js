const express = require('express');
const cors = require('cors');
const helmet = require('helmet');

const router = require('./routes/routes');

const port = process.env.PORT || 3000;

const app = express();

app.use(cors());
app.use(helmet());
app.use('/api',router)

app.use((req,res)=>{
    res.status(404).json({'error':"page doesn't exist"})
})
app.listen(port)
