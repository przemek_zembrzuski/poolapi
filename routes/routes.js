const express = require('express');
const router = express.Router();
const faker = require('faker');
const { firstCapital } = require('../helpers/helpers');


faker.locale = 'pl';

router.get('/getinfo/:name', (req, res) => {
    const json = {
        'user': {
            'name': firstCapital(req.params.name),
            'lastName': faker.name.lastName(),
            'validityDate': faker.date.future(),
            'amount': faker.random.number({
                'min': 0,
                'max': 350
            }),
        },
        'pool': {
            'users': faker.random.number({
                'min': 0,
                'max': 50
            })
        }
    };
    res.status(200).json(json)
})

router.get('/getInfo/', (req, res) => {
    res.status(400).json({ 'error': 'name not given' })
})


module.exports = router;